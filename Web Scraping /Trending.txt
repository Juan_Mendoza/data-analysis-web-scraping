Jason Day, LeBron James, Cleveland Cavaliers, Oklahoma City Thunder, Golf
http://dfw.cbslocal.com/2015/12/18/lebron-james-plows-into-crowd-wife-of-golfer-jason-day-hurt/

Milwaukee Brewers, Pittsburgh Pirates, Jason Rogers
http://espn.go.com/mlb/story/_/id/14392691/pittsburgh-pirates-acquire-jason-rogers-milwaukee-brewers

Volleyball, National Collegiate Athletic Association, NCAA Division I Women's Volleyball Championship, NCAA Men's Division I Basketball Championship
http://www.stltoday.com/sports/tennis/huskers-beat-kansas-in-sets-move-to-ncaa-volleyball-final/article_e2fdd766-ef40-543c-9704-159610373ecd.html

Caitlyn Jenner, Barbara Walters, Barbara Walters' 10 Most Fascinating People
http://www.orlandosentinel.com/entertainment/tv/tv-guy/os-caitlyn-jenner-barbara-walters-donald-trump-20151209-post.html

Confederate States of America, New Orleans
http://www.philly.com/philly/news/nation_world/20151218_New_Orleans_to_remove_Confederate_monuments.html

Mother Teresa, Pope Francis
http://www.al.com/news/index.ssf/2015/12/mother_teresa_to_be_made_saint.html

Dollar Shave Club, Gillette
http://www.foxbusiness.com/industries/2015/12/18/gillette-sues-dollar-shave-club-over-patent/

Tampa, Tampa Bay Buccaneers, Tampa City Council, Raymond James Stadium, Raymond James Financial
http://www.tampabay.com/news/localgovernment/ray-jay-renovation-deal-with-the-bucs-goes-to-tampa-city-council/2258090

Cincinnati Reds, Brandon Phillips, Washington Nationals, MLB
http://washington.cbslocal.com/2015/12/18/report-nationals-agree-to-acquire-brandon-phillips/

GlaxoSmithKline, ViiV Healthcare, Bristol-Myers Squibb, HIV
http://www.telegraph.co.uk/finance/newsbysector/pharmaceuticalsandchemicals/12057415/GlaxoSmithKline-buys-Bristol-Myers-Squibb-HIV-assets-for-up-to-1.46bn.html

Virginia, Islam, School, U.S. County
http://www.huffingtonpost.com/entry/arabic-calligraphy-shuts-schools_56737382e4b06fa6887cdc1f

Bar Refaeli
http://www.hindustantimes.com/hollywood/supermodel-bar-refaeli-mother-arrested-for-tax-evasion/story-gFf1e9SrOnRc9y75CTWTfI.html

Tom Steyer, Washington, California
http://www.seattletimes.com/seattle-news/politics/calif-billionaire-backing-carbon-initiative-in-washington/

Pennsylvania, Islamic State of Iraq and the Levant, Terrorism, Harrisburg
http://english.alarabiya.net/en/News/world/2015/12/18/U-S-cases-targeting-ISIS-supporters-move-ahead-.html

American football, Waskom
http://www.chron.com/sports/highschool/article/Chan-Amie-carries-Waskom-to-back-to-back-state-6706726.php

Texas, American football
http://sportsday.dallasnews.com/high-school/high-schools/2015/12/17/6a-state-teams-arlington-lb-get-defensive-poy-9-locals-first-team

NPR, Michele Norris
http://www.wearegreenbay.com/entertainment/norris-leaving-npr-expanding-race-card-project

Donald Trump, Barbara Walters
http://www.nbcnews.com/politics/2016-election/donald-trump-says-hed-call-himself-loser-if-he-doesnt-n482336

SpaceX, Elon Musk, Mars
http://www.orlandosentinel.com/news/space/go-for-launch/os-spacex-test-fire-delayed-and-launch-of-falcon-9-not-set-20151217-post.html

John Boyega, Han Solo, Harrison Ford, Star Wars: Episode VII
http://www.telegraph.co.uk/film/star-wars-the-force-awakens/john-boyega-buys-nigerian-meal-for-harrison-ford-peckham/

Ranveer Singh, Deepika Padukone, Mastani, Bajirao I, Bajirao Mastani
http://www.ndtv.com/india-news/bomaby-high-court-refuses-to-grant-stay-on-release-of-bajirao-mastani-1256532

AMC Theatres, Movie theater, Ohio
http://www.cbsnews.com/news/states-feds-involved-in-movie-theater-antitrust-inquiries/

Xbox One, Xbox, Video game
http://metro.co.uk/2015/12/17/ark-survival-evolved-xbox-one-preview-riding-with-dinosaurs-5570572/

IBM, AT&T
http://siliconangle.com/blog/2015/12/17/sizing-up-the-public-cloud-ibm-is-an-emerging-application-giant/

Jeff Williams, Tim Cook, Apple Inc.
http://www.independent.co.uk/life-style/gadgets-and-tech/news/apple-reshuffles-board-to-fill-tim-cooks-old-job-and-promotes-most-senior-staff-a6778191.html

Ethan Couch, Affluenza
http://www.dailymail.co.uk/news/article-3365533/You-run-ll-looking-shoulder-Sheriff-fires-warning-affluenza-teen-run-mom-appearing-violate-probation-drinking-FBI-joins-hunt.html

Dennis Hastert, Stroke
http://www.startribune.com/timeline-of-events-in-dennis-hastert-s-life-and-career/362901381/

September 11 attacks, United States Armed Forces, Guantanamo Bay detention camp, Guantanamo Bay Naval Base
http://english.ahram.org.eg/NewsContent/2/9/173821/World/International/US-military-to-limit-media-access-to-Guantanamo-Ba.aspx

Prince William, Duke of Cambridge, Catherine, Duchess of Cambridge, Prince George of Cambridge
http://www.belfasttelegraph.co.uk/video-news/video-prince-william-and-kate-release-new-family-photo-as-george-prepares-for-nursery-34298488.html

Oscar Isaac, Star Wars: Episode VII
http://www.independent.co.uk/arts-entertainment/films/news/star-wars-the-force-awakens-watch-bill-murrays-star-wars-song-covered-by-oscar-isaac-a6776976.html

NFL, Donald Trump
http://sportsday.dallasnews.com/dallas-cowboys/cowboys/2015/12/17/time-make-americas-team-great-see-donald-trumpcowboys-logo-mashup

The Big Bang Theory, Sheldon Cooper
http://www.dailyrecord.co.uk/entertainment/big-bang-theorys-sheldon-amy-7033524

Kathleen Kennedy, Steven Spielberg, Star Wars: Episode VII, Star Wars
http://www.tv3.ie/xpose/article/entertainment-news/187721/Kathleen-Kennedy-holds-Star-Wars-screening-for-film-pals

Matthew Perry, Friends, Courteney Cox
http://www.ibnlive.com/news/buzz/rumour-of-mathew-perry-and-courtney-cox-dating-are-doing-the-rounds-of-the-internet-and-we-really-want-it-to-be-true-1178695.html

Metal detector, Walt Disney, The Walt Disney Company
http://www.iol.co.za/travel/travel-news/us-theme-parks-boost-security-screenings-1.1961650

ExxonMobil
http://midsouthnewz.com/exxon-mobil-co-xom-coverage-initiated-at-credit-agricole/69598/

Holiday, Stress
http://www.huffingtonpost.com/michael-s-broder-phd/divorced-or-separated-with-kids-here-are-3-tips-for-helping-them-through-this-holiday-season_b_8826940.html

National Audubon Society
http://nhv.us/content/15123653-several-bird-counters-are-expected-invade-sabine-wildlife

Mumia Abu-Jamal
http://philadelphia.cbslocal.com/2015/12/18/mumia-abu-jamal-to-testify-at-hearing-on-medical-care/

Carowinds
http://www.thestate.com/living/article50355425.html

New York Jets, Dallas Cowboys, Dez Bryant
http://bleacherreport.com/articles/2600287-dallas-cowboys-vs-new-york-jets-whats-the-gameplan-for-dallas

Lovelady
http://www.khou.com/story/news/crime/2015/12/17/women-pistol-whipped-by-violent-bank-robbers-in-lovelady/77506842/

Will Ferrell, The Tonight Show, Santa Claus
http://www.people.com/article/will-ferrell-new-santa-claus-tonight-show

Florida Atlantic University, College, Sandy Hook Elementary School shooting
http://www.news.com.au/world/north-america/florida-atlantic-university-issues-termination-letter-to-sandy-hook-truther-professor-james-tracy/news-story/fe6fe27aaf6730143b7e935a55357beb

PayPal, Super Bowl, Advertising
http://variety.com/2015/tv/news/super-bowl-ads-paypal-first-time-1201661957/

Qihoo
http://www.wsj.com/articles/chinas-qihoo-360-strikes-new-buyout-deal-1450439749

Rhode Island, Middletown, St. George's School, Newport, School, University-preparatory school
http://www.nytimes.com/2015/12/18/us/prestigious-rhode-island-school-announces-sexual-abuse-investigation.html

Appalachian State Mountaineers football
http://bleacherreport.com/articles/2600115-camellia-bowl-2015-ohio-vs-appalachian-state-tv-schedule-time-and-odds

Seattle Mariners, Hisashi Iwakuma, Los Angeles Dodgers
http://www.latimes.com/sports/dodgers/la-sp-iwakuma-dodgers-coaches-20151218-story.html

Star Wars, Humour
http://www.ctvnews.ca/entertainment/10-ways-star-wars-became-a-pop-culture-fixation-1.2704671

NFL, St. Louis, St. Louis Rams
http://bleacherreport.com/articles/2600271-jeff-fishers-mediocrity-wasting-st-louis-rams-team-loaded-with-young-talent

NCAA Division III Football Championship, NCAA Division III, University of Mount Union, Mount Union Purple Raiders football
http://www.cleveland.com/sports/college/index.ssf/2015/12/mount_union_vs_st_thomas_5_thi.html

Han Solo, Ariana Grande, Star Wars
http://www.wired.com/2015/12/the-physics-in-star-wars-isnt-always-right-and-thats-ok/

Autism, gamma-Aminobutyric acid
http://www.dailymail.co.uk/health/article-3364574/How-simple-visual-test-detect-autism-Brains-children-condition-twice-long-respond-images.html

Toy, BMW
http://www.thestar.com.my/tech/tech-news/2015/12/18/bmw-recalls-baby-racer-toy-car-over-health-hazard/

North Carolina, California Department of Motor Vehicles
http://americablog.com/2015/12/north-carolina-sued-national-voter-registration-act.html

Final Fantasy XV, Square Enix, Enix
http://za.ign.com/final-fantasy-versus-xiii/96592/news/square-enix-asks-if-you-prefer-final-fantasy-15-or

Iman Shumpert, Cleveland Cavaliers, Kyrie Irving
http://www.cleveland.com/cavs/index.ssf/2015/12/cleveland_cavaliers_rely_on_ne.html

Henry County, Georgia, NASA, Georgia
http://www.al.com/news/index.ssf/2015/12/that_big_green_flash_in_the_al.html

Austin, Westlake High School
http://www.dailymail.co.uk/news/article-3365649/Texas-high-school-math-teacher-28-arrested-sexual-contact-17-year-old-students.html

Michigan, Flint
http://www.huffingtonpost.com/entry/flint-lead-poisoning_56732a38e4b06fa6887ca710

United Nations Security Council, Islamic State of Iraq and the Levant, United Nations
http://www.jpost.com/Middle-East/ISIS-Threat/UN-Security-Council-adopts-resolution-to-cut-off-ISIS-funding-437725

Uncharted 4: A Thief's End
http://masterherald.com/uncharted-4-a-thiefs-end-to-highlight-nathan-drakes-backstory/32666/

Netflix, Full House, Fuller House
http://www.cnet.com/news/netflixs-fuller-house-trailer-will-have-you-feeling-nostalgic/

Refugee, Turkey, Amnesty International, European Union
http://english.alarabiya.net/en/News/middle-east/2015/12/18/Syria-imposes-visa-regime-on-Turks-in-retaliatory-move-.html

Los Angeles Lakers, Kobe Bryant, Milwaukee Bucks
http://www.nydailynews.com/sports/basketball/jason-kidd-miffed-bucks-hit-strip-club-los-angeles-article-1.2469217

F Is for Family, Netflix, Bill Burr
https://www.bostonglobe.com/arts/television/2015/12/17/netflix-for-family-animates/WoAtXVLiLjCsnvRVniK93I/story.html

Formula One, Pirelli, Tire
http://www.skysports.com/f1/news/12515/10096662/pirelli-reveal-australia-bahrain-and-china-tyre-selections-under-new-2016-rule

Fargodome, Fargo, NCAA Division I Football Championship, North Dakota State Bison football, North Dakota State University
http://www.startribune.com/ndsu-one-win-from-return-to-fcs-final/362884061/

Japan, Okinawa Prefecture, Caroline Kennedy, John F. Kennedy
http://newsinfo.inquirer.net/748560/us-ambassador-to-japan-kennedy-defends-okinawa-base-plan

Iran, Ruhollah Khomeini, Ali Khamenei
http://www.brecorder.com/component/content/article/70/268908.html?section=7

Real Madrid C.F., Zinedine Zidane, José Mourinho, Rafael Benítez
http://www.iol.co.za/sport/soccer/world-leagues/last-chance-saloon-for-benitez-1.1961958

Dragon's Dogma, Personal computer
http://www.trustedreviews.com/news/dragon-s-dogma-pc-release-date-announced

Oracle Corporation
http://www.livemint.com/Companies/buKg3Eai9vb4OQvQfJ3IrL/Oracle-tops-profit-estimates-amid-shift-to-cloud-products.html

Josh Heupel
http://www.cbssports.com/collegefootball/eye-on-college-football/25417774/missouri-hires-josh-heupel-as-offensive-coordinator

Dearborn Heights
http://www.freep.com/story/news/local/michigan/wayne/2015/12/17/dearborn-heights-police-station-shooting/77473388/

United States Senate, United States Congress, Democratic Party, Harry Reid, Associated Press
http://www.usnews.com/news/politics/articles/2015-12-18/ap-interview-reid-dems-maybe-to-blame-on-gun-bill-losses

Omaha, United States of America
http://www.npr.org/2015/12/17/460149212/in-americas-heartland-building-one-home-for-three-faiths

Netflix, Steven Avery
http://www.latimes.com/entertainment/tv/la-et-st-making-a-murderer-20151217-column.html

Amy Poehler, Tina Fey, Sisters
http://montrealgazette.com/entertainment/celebrity/movie-review-sisters-tina-fey-and-amy-poehler-are-doing-it-by-themselves

Boko Haram, Nigeria
http://www.vanguardngr.com/2015/12/boko-haram-ecowas-urge-west-african-countries-to-ban-use-of-full-face-veils/

Seagate Technology, EVault, Carbonite
http://observeroracle.com/2015/12/17/carbonite-ups-draas-with-us-14m-evault-acquisition.html

Brandeis University, Middlebury College, Ronald D. Liebowitz
https://www.bostonglobe.com/metro/2015/12/17/brandeis-university-names-new-president/62q9kbeBuogmyHJBLJd16N/story.html

Gwendoline Christie, Star Wars: Episode VII, Fashion
http://www.mirror.co.uk/3am/star-wars-gwendoline-christie-perfect-7029246

Philadelphia, Thomas Jefferson University
http://www.philly.com/philly/news/nation_world/362901311.html

2012 Delhi gang rape, Delhi High Court, Delhi
http://atimes.com/2015/12/delhi-gangrape-hc-refuses-to-stop-juveniles-release/

Nicki Minaj, Human rights, Angola
http://www.thedailybeast.com/articles/2015/12/17/nicki-minaj-s-human-rights-problem.html

Buffalo Sabres, Chad Johnson, Anaheim Ducks
http://www.ocregister.com/articles/getzlaf-696617-ducks-game.html

Eurozone, Euro, Greece
http://www.irishexaminer.com/business/eurozone-ministers-back-1bn-payout-for-greece-372219.html

West Virginia, West Virginia University, Marshall University
http://collegebasketball.nbcsports.com/2015/12/17/video-smu-head-coach-larry-brown-makes-his-return/

Aiken, Aiken County, South Carolina, U.S. County
http://www.aikenstandard.com/article/20151217/AIK0102/151219488

Atlanta Braves, Chipper Jones
http://www.usatoday.com/story/sports/mlb/2015/12/17/chipper-jones-returning-to-braves-as-special-assistant/77502458/

C.H. Yoe High School
http://www.chron.com/sports/highschool/article/Cameron-Yoe-RB-Traion-Smith-leaves-game-after-6706158.php

Refugee, Deportation, Jordan, Sudan
http://www.albawaba.com/news/logistics-issues-slow-jordan%E2%80%99s-deportation-800-sudanese-nationals-783024

New Orleans, Pregnancy
http://www.nola.com/crime/index.ssf/2015/12/he_would_have_been_one_of_the.html

United States of America, United States Department of Homeland Security
http://www.stltoday.com/news/national/lawmakers-seek-details-on-vetting-for-those-bound-for-us/article_88185a1b-11bb-5130-b389-84c9ef1f5eab.html

Bessemer
http://wiat.com/2015/12/17/family-of-slain-gas-station-clerk-speaks-out/

E-commerce, United States of America, Alibaba Group, Office of the United States Trade Representative
http://www.channelnewsasia.com/news/technology/alibaba-avoids-being/2357292.html

OneDrive, SharePoint, Konica Minolta
http://www.myprintresource.com/product/12150927/konica-minolta-business-solutions-usa-inc-dispatcher-phoenix-56

Arsenal F.C., Arsène Wenger, Alexis Sánchez
http://www.dailymail.co.uk/sport/football/article-3364368/Jose-Mourinho-virals-Memes-mock-sacked-Chelsea-manager-social-media-users-town-downfall.html

Star Wars: Episode VII, Disney Infinity, The Walt Disney Company
http://www.vancouversun.com/technology/force+awakens+uniquely+invading+disney+infinity+video+game/11596284/story.html

Rockstar Games, iOS, Grand Theft Auto: Liberty City Stories, Grand Theft Auto V
http://www.forbes.com/sites/erikkain/2015/12/17/now-you-can-play-grand-theft-auto-liberty-city-stories-on-your-iphone/

China Southern Airlines, Boeing, Boeing 737
http://www.business-standard.com/article/international/china-southern-orders-boeings-worth-10-bn-115121701477_1.html

New Orleans, Star Wars: Episode VII
http://www.stltoday.com/entertainment/the-latest-parade-caps-new-orleans-star-wars-celebration/article_ffa6170d-a85e-55aa-b804-6b74adb5299d.html

Arvind Kejriwal, Arun Jaitley, Aam Aadmi Party, Delhi District Cricket Association
http://timesofindia.indiatimes.com/india/DDCA-row-AAP-poses-5-questions-to-Jaitley/articleshow/50233827.cms

Ariana Grande, Christmas Day
http://www.irishexaminer.com/breakingnews/entertainment/ariana-grande-releases-surprise-christmas-album-712035.html

Mumford & Sons, Ticket resale, Ticket
http://www.tv3.ie/xpose/article/entertainment-news/187597/Mumford-and-Sons-and-Sir-Elton-John-battle-touts

Hampton Creek
https://www.washingtonpost.com/news/morning-mix/wp/2015/12/18/how-little-just-mayo-took-on-big-egg-and-won/

Cincinnati Bengals, AJ McCarron, Andy Dalton
http://espn.go.com/blog/nflnation/post/_/id/192327/a-j-green-could-be-aj-mccarrons-biggest-weapon-in-first-career-start

Refugee, Jack Markell, Cannabis, Delaware
http://america.aljazeera.com/articles/2015/12/17/marijuana-policy.html

Catherine, Duchess of Cambridge
http://www.huffingtonpost.com/entry/duchess-kate-best-outfits_5671da13e4b0688701dc0cb2?ir=Style&section=style

Christmas Day, Christmas Eve
http://www.straitstimes.com/world/europe/christmas-eve-asteroid-to-cruise-past-harmlessly-astronomers

Boxing, World Boxing Association, HBO, Bryant Jennings
http://espn.go.com/blog/dan-rafael/post/_/id/14821/jennings-ortiz-undergoing-vada-drug-testing

Antti Raanta, Devan Dubnyk, New York Rangers, Mikko Koivu
http://www.calgaryherald.com/sports/devan+dubnyk+returns+mikko+koivu+scores+twice+wild+beat+rangers/11597525/story.html

NFL, Thursday Night Football
https://www.rt.com/sport/326358-nfl-thursday-night-football/

Jazz, Christian McBride, Christmas Day
http://boisestatepublicradio.org/post/christian-mcbrides-christmas-jazz-playlist

Paintsville
http://www.wkyt.com/content/news/Federal-indictment-charges-Paintsville-mayor-with-using-city-funds-to-pay-for-personal-expenses-362840701.html

Olympic Games, Cycling, Bobby Lea
http://www.sport24.co.za/OtherSport/Cycling/International/olympic-cyclist-lea-suspended-for-doping-20151218

José Mourinho, Chelsea F.C.
http://www.irishexaminer.com/breakingnews/sport/14-killer-stats-for-this-weekends-premier-league-action-712070.html

WEWS-TV, Cleveland, News
http://www.newsnet5.com/news/local-news/cleveland-metro/cmha-explore-program-trains-inner-city-youth-to-be-police-officers

Singapore, Singapore Strait
http://news.asiaone.com/news/singapore/6-still-missing-after-collision-spore-strait

KETV, Omaha
http://www.ketv.com/news/library-trustees-approve-2016-budget-with-minimal-cuts/37004020

Marine Corps Base Camp Lejeune, United States Department of Veterans Affairs
http://www.tampabay.com/news/military/veterans/va-grants-automatic-benefits-to-camp-lejeune-vets-with-any-of-8-illnesses/2258233

New Orleans, Smithsonian Institution, Battle of New Orleans, Andrew Jackson
http://www.kansas.com/entertainment/celebrities/article50217855.html

Methamphetamine, Kenosha
http://myfox8.com/2015/12/17/more-than-1-million-worth-of-meth-disguised-as-cocaine-recovered/

Cleveland, Christmas Day
http://www.nydailynews.com/news/national/boy-6-police-chief-day-save-christmas-article-1.2469950

Bill Hader, Ben Schwartz, Star Wars: Episode VII
http://www.nme.com/filmandtv/news/two-us-comedy-actors-revealed-as-voice-consultants/395806

