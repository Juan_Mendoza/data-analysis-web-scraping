#Automate to run 4 times a day. Morning, Afternoon, Evening, Night

# Get Current Trending Stores from Google Trends & Email them to me

# Scraping Google Trends
import sys
from bs4 import BeautifulSoup
#import os
#os.putenv('DISPLAY', ':0.0')
from PyQt4.QtGui import *
from PyQt4.QtCore import *
from PyQt4.QtWebKit import *
#Email Libraries
import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
from email.MIMEBase import MIMEBase
from email import encoders
import time

#Scrape Google Trends
class Render(QWebPage):
	def __init__(self, url):
		self.app = QApplication(sys.argv)
		QWebPage.__init__(self)
		self.loadFinished.connect(self._loadFinished)
		self.mainFrame().load(QUrl(url))
		self.app.exec_()

	def _loadFinished(self, result):
		self.frame = self.mainFrame()
		self.app.quit()

url = 'https://www.google.com/trends/'
r = Render(url)
soup = BeautifulSoup(unicode(r.frame.toHtml()))

stories_list = []

file = open("Trending.txt", "w+")

for stories in soup.find_all("trending-story"):
	ts_title = (stories['story-title']).encode('utf-8')
	ts_url = (stories['news-url']).encode('utf-8')
	stories_list.append(((ts_title), (ts_url)))
	file.write("%s\n%s\n\n" % ((ts_title), (ts_url)))

file.close()

stories_txt = ""
for row in range(len(stories_list)):
	for col in range(2):
		stories_txt = stories_txt + (stories_list[row][col]) + "\n"
		if (col == 1):
			stories_txt += "\n"

#Email the Trending Stories
time_stamp = ("%s %s" % ((time.strftime("%x")),(time.strftime("%I:%M:%S"))))

fromaddr = ""
toaddr = ""

msg = MIMEMultipart()

msg['From'] = fromaddr
msg['To'] = toaddr
msg['Subject'] = "Trending Stories on " + time_stamp

body = stories_txt

msg.attach(MIMEText(body, 'plain'))

filename = "Trending.txt"
attachment = open("/Users/juanmendoza/Desktop/Trending.txt", "rb")

part = MIMEBase('application', 'octet-stream')
part.set_payload((attachment).read())
encoders.encode_base64(part)
part.add_header('Content-Disposition', "attachment; filename= %s" % filename)

msg.attach(part)

server = smtplib.SMTP('smtp.gmail.com', 587)
server.starttls()
server.login(fromaddr, "")
text = msg.as_string()
server.sendmail(fromaddr, toaddr, text)
server.quit()
