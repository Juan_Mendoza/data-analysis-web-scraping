import requests
import json

#Email Libraries
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import time

url = "https://www.google.com/trends/api/stories/latest?cat=all&fi=15&fs=15&geo=US&ri=300&rs=15&tz=480"
resp = requests.get(url)
txt = resp.text
txt = txt[4:]
trends = json.loads(txt)

count = len(trends['storySummaries']['trendingStories'])

trending_stories = []

for i in range(count):
    title = trends['storySummaries']['trendingStories'][i]['title']
    url = trends['storySummaries']['trendingStories'][i]['articles'][0]['url']
    trending_stories.append(((title), (url)))

stories_txt = ""
for row in range(len(trending_stories)):
    for col in range(2):
        stories_txt = stories_txt + (trending_stories[row][col]) + "\n"
        if (col == 1):
            stories_txt += "\n"


#Email the Trending Stories
time_stamp = ("%s %s" % ((time.strftime("%x")),(time.strftime("%I:%M:%S"))))

fromaddr = ""
toaddr = ""

msg = MIMEMultipart()

msg['From'] = fromaddr
msg['To'] = toaddr
msg['Subject'] = "Trending Stories on " + time_stamp

body = stories_txt

msg.attach(MIMEText(body, 'plain'))

server = smtplib.SMTP('smtp.gmail.com', 587)
server.starttls()
server.login(fromaddr, "")
text = msg.as_string()
server.sendmail(fromaddr, toaddr, text)
server.quit()
