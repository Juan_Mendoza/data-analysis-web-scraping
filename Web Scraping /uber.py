# Load all the data from uber.com into one file
# Look for window.sever shit
# Grab all that
# JSON clean
# and get only "products"

from selenium import webdriver
from selenium.webdriver.remote.webelement import WebElement
import json

driver = webdriver.Firefox()
#driver.set_window_size(1440, 900)

# INITIAL URL
driver.get("https://www.uber.com/cities/")
driver.implicitly_wait(10)

# FIND ALL CITIES IN US
citiesNAME = driver.find_elements_by_css_selector(
    '#react-app > div > div > div > div.bg-primary-layer-color.position--relative > div > div > div > div > div.position--relative.z-20.bg-primary-layer-color.pull-app-gutter--left.soft-app-gutter--left.portable-pull-layout-gutter--right.portable-soft-layout-gutter--right > div > div > div:nth-child(1) > ul > li > p > a > span')
citiesLINK = driver.find_elements_by_css_selector(
    '#react-app > div > div > div > div.bg-primary-layer-color.position--relative > div > div > div > div > div.position--relative.z-20.bg-primary-layer-color.pull-app-gutter--left.soft-app-gutter--left.portable-pull-layout-gutter--right.portable-soft-layout-gutter--right > div > div > div:nth-child(1) > ul > li > p > a')

# Need to put all the links and names in an arry
links = []
names = []

for f in range(len(citiesNAME)):
    names.append(citiesNAME[f].text)
    links.append(citiesLINK[f].get_attribute("href"))

UBER = {}

# CITIES
for i in range(0, len(citiesNAME)):

    # DYNAMIC LINKING
    driver.get(links[i])
    driver.implicitly_wait(5)

    # CITY PAGE
    print(names[i])
    options = driver.find_elements_by_css_selector(
        '.product-carousel__tabs .product-carousel__tabs__item a.display--inline-block')

    optionCOUNTER = len(options)
    # SOME LOCATIONS ONLY HAVE ONE SO THERE IS NO TAB, so it returns 0 so the
    # loop never starts
    if len(options) == 0:
        optionCOUNTER = 1
    # END TABLE FOR ONE

    holder = []
    print(optionCOUNTER)
    for k in range(optionCOUNTER):
            if (names[i] == "Tuscaloosa"):
                continue
            # SKIP THE FIRST CLICK, BECAUSE THE FARES ARE ALREADY PRESENT
            if (k != 0):
                # CLICK MECHANICS'
                carouselNext = driver.find_element_by_css_selector(
                    '#react-app > div > div > div > div.bg-primary-layer-color.position--relative > div > div > div > div > div.city__carousel > div > div > div.soft-huge--bottom > div > div.position--absolute.top.right.left.bottom > button.btn.btn--bit.btn--reverse.position--absolute.image-carousel__next.z-20 > i.icon.icon_right-arrow-thin')
                location = carouselNext.location
                # print(location)
                driver.execute_script(
                    "window.scrollTo(0," + str(location['x']) + ")")
                # END CLICK MECHANICS

                # CLICK TO GET NEXT SET OF FARES
                try:
                    carouselNext.click()
                except:
                    print("Error with Carousel" + str(k) + "")
                driver.implicitly_wait(2)
                # END CLICK FARES

            # Get TITLE & PRICES
            fareTitle = driver.find_elements_by_css_selector(
                '.city__carousel .fare-breakdown .fare-breakdown__item .float--left')
            farePrice = driver.find_elements_by_css_selector(
                '.city__carousel .fare-breakdown .fare-breakdown__item .float--right')

            # FARES
            try:
                serviceTitle = options[k].text
            except:
                serviceTitle = driver.find_element_by_xpath(
                    '//*[@id="react-app"]/div/div/div/div[2]/div/div/div/div/div[3]/div/div/div[3]/div/div[3]/div/div/div[3]/div/span/div/div[1]/h3').text

            fares = {serviceTitle: []}
            fare = {}
            try:
                for j in range(len(fareTitle)):
                    if not (fareTitle[j].text == "" or farePrice[j] == ""):
                        fare[fareTitle[j].text] = farePrice[j].text
            except Exception:
                print("ERROR")

            fares[serviceTitle].append(fare)
            # SAVE TO ARRAY OUTSIDE OF LOOP
            holder.append(fares)
            # END FARES

    UBER[names[i]] = holder

with open("/Users/juanmendoza/Desktop/test.txt", "w+") as out_file:
    json.dump(UBER, out_file)

driver.quit()

# Remove the white spaces
